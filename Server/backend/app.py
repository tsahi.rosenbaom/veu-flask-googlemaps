import uuid

from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
from flask_redis import FlaskRedis
import json

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

Center =  { 'lat': 45.508, 'lng': -73.42, }
Marker = [ { 'index': uuid.uuid4().hex, 'position': {'lat': 45.51, 'lng': -73.40, }, }]
Markers = [
    {
        'index': uuid.uuid4().hex,
        'position': {'lat': 45.51, 'lng': -73.40, },
    },
    {
        'index': uuid.uuid4().hex,
        'position': {'lat': 45.5093, 'lng': -73.41, },
    },
    {
        'index': uuid.uuid4().hex,
        'position': {'lat': 45.5087, 'lng': -73.42, },
    },
    {
        'index': uuid.uuid4().hex,
        'position': {'lat': 45.508, 'lng': -73.43, },
    },
    {
        'index': uuid.uuid4().hex,
        'position': {'lat': 45.5075, 'lng': -73.44, },
    },
    {
        'index': uuid.uuid4().hex,
        'position': {'lat': 45.5067, 'lng': -73.45, },
    }
]

@app.route('/map', methods=['GET'])
@cross_origin()
def map():
    response_object = {'status': 'success'}
    response_object['center'] = Center
    response_object['markers'] = Marker
    return jsonify(response_object)

count = 0
@app.route('/markers/<marker_id>',methods=['GET'])
@cross_origin()
def markers(marker_id):
    global count
    if count == 5:
        count = 0
    else:
        count = count + 1
    response_object = {'status': 'success'}
    response_object['marker'] = Markers[count]
    return jsonify(response_object)


if __name__ == '__main__':
    app.run()