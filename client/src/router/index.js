import Vue from 'vue';
import VueRouter from 'vue-router';
import Maps from '../components/GoogleMap.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Maps',
    component: Maps,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
