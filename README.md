# Docker Flask Vue GoogleMap

### Based on


[Developing a Single Page App with Flask and Vue.js](https://testdriven.io/blog/developing-a-single-page-app-with-flask-and-vuejs/#put-route)

[Using Google Maps in Vue with vue-google-maps](https://www.digitalocean.com/community/tutorials/vuejs-vue-google-maps)

[Vue.js polling using setInterval()](https://renatello.com/vue-js-polling-using-setinterval/)

[Deploying a Flask and Vue App to Heroku with Docker and Gitlab CI](https://testdriven.io/blog/deploying-flask-to-heroku-with-docker-and-gitlab/)  



### Build & Launch

```bash
docker build -t web:latest .
docker run -d --name flask-vue-map -e "PORT=8765" -p 8007:8765 web:latest
```

To shut down:

```bash
docker stop flask-vue-map
docker rm flask-vue-map
```

